import React from 'react'
import {
    StyleSheet,
    Image
} from 'react-native'
import HtmlRender from 'react-native-html-render'

export default class Html extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        const { html } = this.props
        const sample = "<h1>你好</h1>"

        return (
            html ?
            <HtmlRender
                value={sample}
                stylesheet={styles}
            /> : null
        )
    }
}

const styles = StyleSheet.create({
    img: {
        width: 100,
        height: 100
    }
})
