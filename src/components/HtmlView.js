import React from "react"
import HTMLView from "react-native-htmlview"

export default class HtmlView extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        const { html, styles } = this.props

        const _renderNode = (node, index, siblings, parent, defaultRenderer) => {
            if(node.name === 'img') {
                const name = node.name
                return null
            }
        }

        return <HTMLView value={html} stylesheet={styles} renderNode={_renderNode} />
    }
}
