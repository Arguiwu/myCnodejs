import axios from 'axios'
import qs from 'qs'
import { Toast } from 'antd-mobile'

axios.defaults.baseURL = '1'
axios.defaults.headers.post['Content-Type'] = 'application/json'
axios.defaults.headers.common['lang'] = 'zn'

axios.interceptors.response.use(function(res) {
	var code = res.data.code;
	var message = res.data.message;
	return res;
}, function(err) {
	return Promise.reject(err)
})

const fetch = (options) => {
	if(!options.isWai) {
		options.url = options.url + '.php'
	}
	let {
		method = 'get',
		data,
		url,
		timeout = 30000
	} = options;

	switch(method.toLowerCase()) {
		case 'get':
			if(data) {
				return axios.get(`${url}?${qs.stringify(data)}`);
			}else {
				return axios.get(`${url}`);
			}
			break;
		case 'post':
			return axios.post(url, data);
	}
};

export default function request(options) {
	axios.defaults.headers.common['TOKEN'] = '1'
	axios.defaults.headers.common['PAYPWD'] = ''
	
	Toast.loading('加载中...', 30000)
	return fetch(options).then((response) => {
		const { data } = response;
		Toast.hide()
		if(data && data.status == 'success') {
			
		}else {
			Toast.info(data.message, 1.5)
		}
		return  {
			...data
		}
	}).catch( () => {
		Toast.fail('请求繁忙，请稍候...', 1.5);
		return {
			code: 0,
			message: '请求繁忙，请稍候...'
		}
	})
};